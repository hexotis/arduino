#include <SoftwareSerial.h>   //Software Serial Port

#define RxD         4
#define TxD         5

#define Ordres      13
#define Tcapteur    12

#define ENVOICOURANT()     digitalWrite(Ordres, HIGH)
#define COUPERCOURANT()    digitalWrite(Ordres, LOW)

#define DEBUG_ENABLED  1

SoftwareSerial blueToothSerial(RxD,TxD);


void setup() {
 
    Serial.begin(9600);
    pinMode(RxD, INPUT);
    pinMode(TxD, OUTPUT);
    pinMode(Ordres, OUTPUT);
    pinMode(Tcapteur, INPUT);
    COUPERCOURANT();
    
    setupBlueToothConnection();

  
 }

 void setupBlueToothConnection()
{ 

  
  
  blueToothSerial.begin(9600);  
  
  blueToothSerial.print("AT");
  delay(400); 

  blueToothSerial.print("AT+DEFAULT");             // Restore all setup value to factory setup
  delay(2000); 
  
  blueToothSerial.print("AT+NAMESeeedBTSlave");    // set the bluetooth name as "SeeedBTSlave" ,the length of bluetooth name must less than 12 characters.
  delay(400);
  
    blueToothSerial.print("AT+PIN0000");             // set the pair code to connect 
  delay(400);
  
  blueToothSerial.print("AT+AUTH1");             //
    delay(400);    

    blueToothSerial.flush();

}  


void loop(){
  
  Tcapteur == digitalRead(12);     
  Serial.print(Tcapteur);

  char Tdemandee; 

  char Lettre = Serial.read();
  Serial.print(Lettre);

  while(1)
  {
    if(blueToothSerial.available()) // Chercks for BT signal
      {
        Lettre = blueToothSerial.read();
        Serial.print(Lettre);
        
        if (Lettre == 'C')
        {
          Tdemandee = blueToothSerial.read();
            if (Tdemandee >= Tcapteur)
            {
              ENVOICOURANT();
            }
            else
            {
              COUPERCOURANT();
            } 
        }
        else if(Lettre == 'F')
        {
          Tdemandee = blueToothSerial.read();
            if (Tdemandee<Tcapteur)
            {
               ENVOICOURANT();
            }
            else
            {
              COUPERCOURANT();
            }
        }
      }
  }
}

    

  
  



