/*
  Carte 1 ArduinoMega
  Les ports de sorties midi sont les RX/TX
  Notes 0 à 7
  Temps d'exec = [300us;400us]
*/
//bibliothèques
#include <MIDI.h>                   //biblioteque du Midi


#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))                     //accelere l'adc arduino
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

/*  3.3v = 676  c'est le 0
    5v   = 1024 c'est le max
    1024-676=348 donc KvD= 0.3649
*/
#define KvD 0.3649      //constante de convertion pour passer du port analogique(sur 1024) à la vélocité (sur 127)
#define LED 13          //Pin de la led de l'arduino (led d'info)

//pour le midi
#define Chl1 1
#define Chl2 2

//pour la lecture des ports
int Piezo[16];         //declaration de variables des 8 capteurs (0 à 7)
boolean N[16];         //Etat du capteur
int SeuilD = 700;     //seuil front montant
int SeuilF = 2;      //seuil front descendant
double t = 0;          //temps
byte LedCount = 0;    //allumage de la led

//Pour les SendNotes
byte Note;              //note jouée
byte Octave = 4;        //octave de jeu, de 0(grave) à 7(aigu)
byte Velocity = 0;      //force d'appui (niveau de son)

// pour TransmitToC2
byte Data;
int NoteT;
byte VelocityT;

MIDI_CREATE_DEFAULT_INSTANCE();               //configure le midi

//---------------------------------------------------------

//setup loop
void setup() {
  for (byte a = 0; a < 16; a = a + 1) {       //initialise les variables N[i] à 0
    N[a] = 0;
  }
  sbi(ADCSRA, ADPS2);                         //accelere l'adc arduino
  cbi(ADCSRA, ADPS1);
  cbi(ADCSRA, ADPS0);
  pinMode(LED, OUTPUT);                       //pin de la led d'info
  MIDI.begin(1);                              //initialise le port midi //MIDI_CHANNEL_OMNI
  //Serial.begin(115200);//Debug
  Serial1.begin(115200);                      //initialize serial communication at 115200 bits per second (31250 pour le midi?)
}

//---------------------------------------------------------

//Assignation des touches de la C1
byte InputMapC1(byte i)  {
  switch (i) {
    case 0:
      i = 7;
      break;
    case 1:
      i = 9;
      break;
    case 3:
      i = 11;
      break;
    case 4:
      i = 10;
      break;
    case 6:
      i = 5;
      break;
    case 7:
      i = 6;
      break;
    case 8:
      i = 4;
      break;
    case 9:
      i = 1;
      break;
    case 11:
      i = 3;
      break;
    case 12:
      i = 0;
      break;
    case 14:
      i = 2;
      break;
    case 15:
      i = 8;
      break;
  }
  return i;
}

//Assignation des touches de la C2
byte InputMapC2(byte Data)  {
  switch (Data) {
    case 0:
      Data = 10;
      break;
    case 1:
      Data = 11;
      break;
    case 3:
      Data = 7;
      break;
    case 4:
      Data = 9;
      break;
    case 6:
      Data = 8;
      break;
    case 7:
      Data = 5;
      break;
    case 8:
      Data = 0;
      break;
    case 9:
      Data = 2;
      break;
    case 11:
      Data = 4;
      break;
    case 12:
      Data = 3;
      break;
    case 14:
      Data = 1;
      break;
    case 15:
      Data = 6;
      break;
  }
  return Data;
}

//Fonction d'envoi de noteOn de C1 à CMozzi
void SendNoteOn(byte i, byte Octave) {
  N[i] = 1;                                   //retient que la note est activée
  Note = Octave * 12 + InputMapC1(i) + 24;           //Defini la note qui vient d'etre jouée (en midi)
  Velocity = Piezo[i] * KvD;                  //vélocité pour l'intensité de la note
  ++LedCount;                    //allume la led
  MIDI.sendNoteOn(Note, Velocity, Chl1);      //Envoi NoteOn sur le chl2
  //Serial.print("+"); Serial.print(i); Serial.print(" ");//Debug
  //Serial.println();//Debug
}

//Fonction d'envoi de noteOff de C1 à CMozzi
void SendNoteOff(byte i, byte Octave) {
  N[i] = 0;                                     //retient que la note est activée
  Note = Octave * 12 + InputMapC1(i) + 24;                 //Defini la note qui vient d'etre jouée (en midi)
  Velocity = 0;                                 //vélocité pour l'intensité de la note
  --LedCount;                       //allume la led
  MIDI.sendNoteOff(Note, Velocity, Chl1);       //Envoi NoteOff sur le chl1
  //Serial.print("-"); Serial.print(i); Serial.print(" ");//Debug
  //Serial.println();//Debug
}

//Fonction de transmition de la C2 à C1 à CMozzi
void TransmitToCmozzi(byte Octave) {
  if (Serial1.available() > 0)  {                            //Si il y a au moins un octet reçu...
    byte Data = Serial1.read();                                   //acquérit l'octet
    if  (Data > 127)  {                                      //Si c'est une  Note.. car >B10000000 (bit de poids fort=1)
      NoteT = Octave * 12 + InputMapC2(Data - B10000000) + 24 + 12;                           //Definit la note
      delayMicroseconds(100);
      VelocityT = Serial1.read();                            //vélocité pour l'intensité de la note
      if (VelocityT != 0) {                                  //Si NoteOn
        ++LedCount;
        MIDI.sendNoteOn(NoteT, VelocityT, Chl1);             //NoteOn
        //Serial.print(" ");  Serial.print(Data);Serial.print(" ");Serial.print(NoteT); Serial.print("+");Serial.print(VelocityT);//Debug
        //Serial.println();//Debug
      }
      else {                                                //Sinon NoteOff
        --LedCount;
        MIDI.sendNoteOff(NoteT, VelocityT, Chl1);
        //Serial.print(" ");  Serial.print(Data);Serial.print(" ");Serial.print(NoteT); Serial.print("-");Serial.print(VelocityT);//Debug
        //Serial.println();Serial.println();//Debug
      }
    }
  }
}

//---------------------------------------------------------

//main loop
void loop() {
  t = micros();                               //enregistre le temps au debut de la boucle
  TransmitToCmozzi(Octave);
  for (byte i = 0; i < 16; i = i + 1) {        //Pour les  capteur ....
    if ((i == 2) || (i == 5) || (i == 10) || (i == 13)) { //sauter les ports avec le GND
      ++i;
    }
    Piezo[i] = analogRead(i);                 //lit le capteur
    if ((Piezo[i] >= SeuilD) && (!N[i])) {    //...detecte si la touche est apuyée
      SendNoteOn( i,  Octave);                //noteOn
    }
    if ((Piezo[i] <= SeuilF) && (N[i])) {     //...detecte si la touche est relachée
      SendNoteOff( i,  Octave);               //noteOff
    }
    if (LedCount != 0) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
  }
  t = micros() - t;                           //calcule le temps pour faire la boucle en us
  //Serial.println(t);//Debug
}
