//bibliothèques
#include <Velleman_KA12.h>

int Piezo[24];      //declaration de variables des 24 capteurs (0 à 23)
int SeuilD = 850;       //seuil front montant
int SeuilF = 500;       //seuil front descendant

int Octave = 4;       //octave de jeu, de 0(grave) à 7(aigu)
int Note[88];           //note jouée


void setup() {
  //carte initialise la carte d'acquisition
  ka12_init();
  // initialize serial communication at 115200 bits per second:
  Serial.begin(115200);
}


void loop() {

  ka12_readAll(Piezo);         // lit les valeurs des port analogiques 0 à 23 (tous)

  for (int i = 0; i < 24; i = i + 1) {
    if (Piezo[i] >= SeuilD) {
      Note[Octave * 12 + i + 24];         //Defini la note qui vient d'etre jouée (en midi)

      Serial.print("  |||"); Serial.print(i); Serial.print("|||  ");
    }
    Serial.print(Piezo[i]); Serial.print("--");     // affiche toutes les valeurs des ports 0 à 23 dans le serial
  }
  Serial.println();     //saute une ligne

  delayMicroseconds(20);     // delay de 20us pour la stabilité
}




