/*Carte 1 ArduinoMega
  Les ports de sorties midi sont les RX/TX
  Notes 8 à 23
  min 400
*/


#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))                     //accelere l'adc arduino
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

/*
    3.3v = 676  c'est le 0
    5v   = 1024 c'est le max
    1024-676=348 donc KvD= 0.3649
*/
#define KvD 0.3649      //constante de convertion pour passer du port analogique(sur 1024) à la vélocité (sur 127)
#define LED 13          //Pin de la led de l'arduino (led d'info)


int Piezo[16];           //declaration de variables des 24 capteurs (0 à 23)
boolean N[16];          //Etat du capteur
int SeuilD = 500;      //seuil front montant
int SeuilF = 2;       //seuil front descendant
double t = 0;           //temps
byte LedCount = 0;

byte Data;              //donnée transmie
byte Velocity = 0;      //force d'appui (niveau de son)

//---------------------------------------------------------

//setup loop
void setup() {
  for (byte a = 0; a < 16; a = a + 1) {    //initialise les variables N[i] à 0
    N[a] = 0;
  }
  sbi(ADCSRA, ADPS2);                      //accelere l'adc arduino
  cbi(ADCSRA, ADPS1);
  cbi(ADCSRA, ADPS0);
  pinMode(LED, OUTPUT);                    //pin de la led d'info
  Serial.begin(115200);                    //initialize serial communication at 115200 bits per second (31250 pour le midi?)
}

//---------------------------------------------------------

void SendPiezoOn (byte i) {
  Data = i + B10000000;                   //Defini la note qui vient d'etre jouée (en midi)
  N[i] = 1;                               //retient que la note est activée
  ++LedCount;
  Velocity = Piezo[i] * KvD;              //vélocité pour l'intensité de la note
  Serial.write(Data);                     //Envoi du numéro du piezo
  delayMicroseconds(40);
  Serial.write(Velocity);                 //envoi de la vélocité
  //Serial.println();
}

void SendPiezoOff (byte i) {
  Data = i + B10000000;                   //Defini la note qui vient d'etre jouée (en midi)
  N[i] = 0;                               //retient que la note est activée
  --LedCount;
  Velocity = 0;                           //vélocité pour l'intensité de la note
  Serial.write(Data);                     //Envoi du numéro du piezo
  delayMicroseconds(40);
  Serial.write(Velocity);                 //envoi de la vélocité
  //Serial.println();
}

//---------------------------------------------------------

//main loop
void loop() {
  t = micros();                            //enregistre le temps au debut de la boucle
  for (byte i = 0; i < 16; i = i + 1) {     //Pour les 24 capteur ....
    if ((i == 2) || (i == 5) || (i == 10) || (i == 13)) { //sauter les ports avec le GND
      ++i;
    }
    Piezo[i] = analogRead(i);              //lit le capteur
    if ((Piezo[i] >= SeuilD) && (!N[i])) { //...detecte si la touche est jouée
      SendPiezoOn(i);                     //Envoi le message midi d'activation
      //Serial.print(" ");  Serial.print(i); Serial.print("+");Serial.println(Velocity);//Debug
    }
    if ((Piezo[i] <= SeuilF) && (N[i])) {  //...detecte si la touche est jouée
      SendPiezoOff(i);                    //Envoi le message midi de désactivation
      //Serial.print(" ");  Serial.print(i); Serial.print("-");Serial.println(Velocity);//Debug
    }
    if (LedCount != 0) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
  }
  t = micros() - t;                        //calcule le temps pour faire la boucle en us
  //Serial.println(t);//Debug
}
