//Carte 2
/*
  Com sur RX/TX
  Sons sur le port 9
*/
#include <MIDI.h>                   //biblioteque du Midi

#include <MozziGuts.h>              //biblioteque Mozzi
#include <Oscil.h>                  // oscillator template
#include <tables/sin2048_int8.h>    // sine table for oscillator
#include <mozzi_midi.h>             //fonctions midi (mtof (l 55)
#include <ADSR.h>                   //envelope

//configure le midi
MIDI_CREATE_DEFAULT_INSTANCE();


// audio sinewave oscillators
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin1(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin2(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin3(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin4(SIN2048_DATA);

// envelope generators
ADSR <CONTROL_RATE, AUDIO_RATE> envelope1;
ADSR <CONTROL_RATE, AUDIO_RATE> envelope2;
ADSR <CONTROL_RATE, AUDIO_RATE> envelope3;
ADSR <CONTROL_RATE, AUDIO_RATE> envelope4;

#define LED 13 // shows if MIDI is being recieved

// use #define for CONTROL_RATE, not a constant
#define CONTROL_RATE 256 // powers of 2 please
#define ATTACK 15
#define DECAY 15
#define SUSTAIN 60000 // Sustain 60 seconds unless a noteOff comes.
#define RELEASE 200
#define ATTACK_LEVEL 64
#define DECAY_LEVEL 64
#define SUSTAIN_LEVEL 64
#define RELEASE_LEVEL 50

// notes
byte note1 = 0;
byte note2 = 0;
byte note3 = 0;
byte note4 = 0;


//---------------------------------------------------------------

void HandleNoteOn(byte channel, byte note, byte velocity) {
  if (note1 == 0) {
    note1 = note;
    aSin1.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(note)));
    envelope1.noteOn();
  }
  else if (note2 == 0) {
    note2 = note;
    aSin2.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(note)));
    envelope2.noteOn();
  }
  else if (note3 == 0) {
    note3 = note;
    aSin3.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(note)));
    envelope3.noteOn();
  }
  else if (note4 == 0) {
    note4 = note;
    aSin4.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(note)));
    envelope4.noteOn();
  }
  if (note1 + note2 + note3 + note4 != 0) {
    digitalWrite(LED, HIGH); // no notes playing
  }
}

void HandleNoteOff(byte channel, byte note, byte velocity) {
  if (note == note1) {
    envelope1.noteOff();
    note1 = 0;
  }
  else if (note == note2) {
    envelope2.noteOff();
    note2 = 0;
  }
  else if (note == note3) {
    envelope3.noteOff();
    note3 = 0;
  }
  else if (note == note4) {
    envelope4.noteOff();
    note4 = 0;
  }
  if (note1 + note2 + note3 + note4 == 0) {
    digitalWrite(LED, LOW); // no notes playing
  }
}

//----------------------------------------------


void setup() {
  pinMode(LED, OUTPUT);//config de la led

  // Connect the HandleNoteOn function to the library, so it is called upon reception of a NoteOn.
  MIDI.setHandleNoteOn(HandleNoteOn);
  MIDI.setHandleNoteOff(HandleNoteOff);

  MIDI.begin(1);  // Initiate MIDI communications, listen to channel 1

  envelope1.setLevels(ATTACK_LEVEL, DECAY_LEVEL, SUSTAIN_LEVEL, RELEASE_LEVEL);
  envelope1.setTimes(ATTACK, DECAY, SUSTAIN, RELEASE);
  envelope2.setLevels(ATTACK_LEVEL, DECAY_LEVEL, SUSTAIN_LEVEL, RELEASE_LEVEL);
  envelope2.setTimes(ATTACK, DECAY, SUSTAIN, RELEASE);
  envelope3.setLevels(ATTACK_LEVEL, DECAY_LEVEL, SUSTAIN_LEVEL, RELEASE_LEVEL);
  envelope3.setTimes(ATTACK, DECAY, SUSTAIN, RELEASE);
  envelope4.setLevels(ATTACK_LEVEL, DECAY_LEVEL, SUSTAIN_LEVEL, RELEASE_LEVEL);
  envelope4.setTimes(ATTACK, DECAY, SUSTAIN, RELEASE);

  startMozzi(CONTROL_RATE);//lance mozzi

}

//---------------------------------------------------


void updateControl() {
  MIDI.read();//lire si on recoit du midi
  envelope1.update();
  envelope2.update();
  envelope3.update();
  envelope4.update();//Updates the internal controls of the ADSR
}


int updateAudio() {
  return (int) (envelope1.next() * aSin1.next() + envelope2.next() * aSin2.next()
                + envelope3.next() * aSin3.next() + envelope4.next() * aSin4.next()) >> 2;//gere l'evolution du son dans l'ADSR et les phases
}


void loop() {
  audioHook(); // required here
}



