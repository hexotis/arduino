//Carte 2
/*
  Com sur RX/TX
  Sons sur le port 9
*/
#include <MIDI.h>                   //biblioteque du Midi

#include <MozziGuts.h>              //biblioteque Mozzi
#include <Oscil.h>                  // oscillator template
#include <Line.h>
#include <tables/sin2048_int8.h>    // sine table for oscillator
#include <mozzi_midi.h>             //fonctions midi (mtof (l 55)
#include <ADSR.h>                   //envelope
#include <mozzi_fixmath.h>

MIDI_CREATE_DEFAULT_INSTANCE();               //configure le midi

// use #define for CONTROL_RATE, not a constant
#define CONTROL_RATE 256 // powers of 2 please

// audio sinewave oscillator
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin1(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin2(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin3(SIN2048_DATA);

// envelope generator ( ADSR for Attack,Decay,Sustain,Release)
ADSR <CONTROL_RATE, AUDIO_RATE> envelope1;
ADSR <CONTROL_RATE, AUDIO_RATE> envelope2;
ADSR <CONTROL_RATE, AUDIO_RATE> envelope3;

#define LED 13 // shows if MIDI is being recieved

byte lastnote1 = 0;
byte lastnote2 = 0;
byte lastnote3 = 0;
int nextenv = 1;

//---------------------------------------------------------------

void HandleNoteOn(byte channel, byte note, byte velocity) {
  if (nextenv == 1) {
    lastnote1 = note;
    envelope1.noteOn();
    aSin1.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(lastnote1))); // accurate frequency
    nextenv = 2;

  }
  else if (nextenv == 2) {
    lastnote2 = note;
    envelope2.noteOn();
    aSin2.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(lastnote2)));
    nextenv = 3;
  }
  else if (nextenv == 3) {
    lastnote3 = note;
    envelope3.noteOn();
   aSin3.setFreq_Q16n16(Q16n16_mtof(Q8n0_to_Q16n16(lastnote3)));
    nextenv = 1;
  }
  digitalWrite(LED, HIGH);
}

void HandleNoteOff(byte channel, byte note, byte velocity) {
  digitalWrite(LED, LOW); // may as well switch LED off
  if ( (note == lastnote1) ) //kill envelope if prev note released
  {
    envelope1.noteOff();
  }

  if ( (note == lastnote2) )
  { envelope2.noteOff();
  }

  if ( (note == lastnote3) )
  { envelope3.noteOff();
  }
}

//----------------------------------------------


void setup() {
  pinMode(LED, OUTPUT);//config de la led

  // Connect the HandleNoteOn function to the library, so it is called upon reception of a NoteOn.
  MIDI.setHandleNoteOn(HandleNoteOn);
  MIDI.setHandleNoteOff(HandleNoteOff);

  MIDI.begin(1);  // Initiate MIDI communications, listen to channel 1

  envelope1.setADLevels(127, 100);
  envelope1.setTimes(20, 20, 20000, 1200); // 20000 is so the note will sustain 20 seconds unless a noteOff comes
  envelope2.setADLevels(127, 100);
  envelope2.setTimes(20, 20, 20000, 1200); // 20000 is so the note will sustain 20 seconds unless a noteOff comes
  envelope3.setADLevels(127, 100);
  envelope3.setTimes(20, 20, 20000, 1200);

   // default frequency
  startMozzi(CONTROL_RATE);//lance mozzi

}

//---------------------------------------------------


void updateControl() {
  MIDI.read();//lire si on recoit du midi
  envelope1.update();//Updates the internal controls of the ADSR
  envelope2.update();
  envelope3.update();
}


int updateAudio() {
  return (int) (
    envelope1.next() * aSin1.next() + envelope2.next() * aSin2.next() + envelope3.next() * aSin3.next()  ) >> 8;//gere l'evolution du son dans l'ADSR et les phases
    }
}


void loop() {
  audioHook(); // required here
}
